  /*
 *  Прошивка для платы Arduino UNO в лабороторный стробоскоп
 *          -//-               Pro Mini Atmega368 5v  -//-
 *  Первоначальный вариант: ДПМ-16-1м, AlexKaz alexkazancev@bk.ru, Март-Апрель 2017
 *                          Частично собрано из открытых исходников в Интернет
 *  Лицензию библиотек см. в их исходниках
 */

// If this code is usuful for you donate please, https://paypal.me/alvlk , https://yoomoney.ru/to/410019721796762

#define useCyberLib
#if defined(useCyberLib)
  #include <CyberLib.h> //библиотека оживляет контроллер, в т.ч. работает с таймером
#endif
//#define DEBUG  //режим отладки - вывод сообщений в консоль

#include <LiquidCrystal.h> //библиотека дисплея, ниже его пины
LiquidCrystal lcd(7, 6, 5, 4, 3, 2); // (RS, E, DB4, DB5, DB6, DB7)

//#define timeMLS // таймер в миллисекундах, т.е. до 500Гц
#if defined (timeMLS)
 #include <MsTimer2.h>
#else
 #define timeMCS  // таймер в микросекундах из CyberLib, т.е. до 1? МГц
#endif

const int LledPin = 13;      //порт светодиода
//Энкодер
const int EncBut = 10;//8; //2 //кнопка 
const int EncCLK = 11;//9; //3;//CLK подключаем к пину digital 3 на плате Arduino
const int EncDT  = 12;// 10; //4;  //DT  подключаем к пину digital 4 на плате Arduino
//Кнопки - Пин analog A0
const int ButPins[] = {A0, A0, A0};     //порты аналоговых кнопок
const int ButMainFreq  = 0; //смещения в массиве ButPins
const int ButChangeReg = 1; //отвечают за свои номера кнопок
const int ButPodstFreq = 2;
const int isPUSHED[]={1023, 523, 500}; //пороговый уровень сигнала на аналоговом порте при нажатии кнопки

const int delayValue = 350; //задержка, мс после настройки кнопками
const float PodstrLevel = 0.1; //изменяем основные частоты на 1/10 при подстройке

unsigned long dT[]={0,0,0};// матрица временных отсчётов для энкодера
//byte dTi=0; //указатель на текущую позицию в энкодере

double Chastoty[]={1, 1, 1, 1, 1, 1};   //основные частоты
int OsnChastot = 1; //всего частот запомнено
const byte MaxTableChastoty=6;

double ch=1.; //текущая частота
int nomerChastoty=1;   //текущая основная частота
int ButsState[]={0, 0, 0}; //заполняются в цикле чтения состояния кнопок

#define regZAPOL 1
#define regPEREB 0

byte regim=regZAPOL; //1 - заполняем таблицу частот, 0 - работаем только с таблицей частот, перебираем
             //режим меняется второй кнопной, из режима 0 возврата нет 
int Podstroyka=0; //от -maxMnogitel до maxMnogitel
int PodPodstroyka=0; //от -maxMnogitel до maxMnogitel
const int maxMnogitel = 5; //получим диапазон регул-я=осн.частота*(1+1/10*Podstroyka)

//Временные переменные для хранения уровней сигналов, полученных от энкодера
unsigned char encoder_A, encoder_B, encoder_A_prev;
//Переменная для отслеживания нажатий кнопки - центральной оси энкодера
static bool SW_State = false;
//переменная для инициации пересчёта частоты таймера
static bool isChanged = false; 

unsigned int isNomerOneMore = 0;

const int DisplayResetPeriod = 2000; //millisec


void LCDprintStart(){
  lcd.setCursor(0, 0);              // Устанавливаем курсор в начало 1 строки 
  lcd.print(" f =         Hz  ");       // Выводим текст
  lcd.setCursor(0, 1);              // Устанавливаем курсор в начало 2 строки
  lcd.print(" T =         mcs  ");       // Выводим текст  
}

//обновление состояния дисплея
void LCDprint(float perem0){
//lcd.clear();
//lcd.home();
//lcd.print(perem0);
  byte smesh1=8;
  if (perem0 >= 10) smesh1--;
  if (perem0 >= 100) smesh1--;
  if (perem0 >= 1000) smesh1--;
  if (perem0 >= 10000) smesh1--;
  long int p=2*Period(perem0);
  byte smesh2=11;
  if (p >= 10) smesh2--;
  if (p >= 100) smesh2--;
  if (p >= 1000) smesh2--;
  if (p >= 10000) smesh2--;
  if (p >= 100000) smesh2--;
  if (p >= 1000000) smesh2--;
  if (p >= 10000000) smesh2--;

  lcd.setCursor(4,0);
  lcd.print("         ");
  lcd.setCursor(4,1);
  lcd.print("         ");

  lcd.setCursor(smesh1,0);
  lcd.print(perem0);
  lcd.setCursor(smesh2,1);
  lcd.print(p);       // Выводим текст
}

//программная перезагрузка
void(* resetFunc) (void) = 0;

//посылаем импульс
void flash() {
  static boolean output = HIGH;
  #if defined(useCyberLib) 
    if (output==HIGH) {D13_High;}
    else D13_Low;
  #else
    digitalWrite(LledPin, output);
  #endif
  output = !output;
  
  #if defined(DEBUG)
    //Serial.println(output);
  #endif
}

//настраиваем таймер
void timerSet(unsigned long delay0){
 
 #if defined(timeMLS)
  MsTimer2::set(delay0, flash); // период и функция
  MsTimer2::start();
 #else
  StartTimer1( flash , delay0);  
 #endif
}

//останавливаем таймер
void timerUnSet() {
 #if defined(timeMLS)
  MsTimer2::stop();
 #else
  StopTimer1();  
 #endif
}

//инициализация устройства
void setup() {
  #if defined(useCyberLib)
    D13_Out;
    //A0_In;
  #else
    pinMode(LledPin, OUTPUT);             //резервируем порты для вывода и ввода
    pinMode(ButPins[ButMainFreq], INPUT);
  #endif
  lcd.begin(16, 2);                  // Задаем размерность экрана
  lcd.setCursor(0, 0);              // Устанавливаем курсор в начало 1 строки
  lcd.print("CTAPT!");       // Выводим текст
    LCDprintStart();
  
  #if defined(DEBUG)
    Serial.begin(9600);                  //настриваем консоль для отладки    Serial.println(output);
    Serial.print("Ch "); Serial.print(ch); Serial.println(" ");
  #endif
  
//  ch=Chastoty[nomerChastoty-1]*(1+PodstrLevel*Podstroyka);
  ch=Chastoty[nomerChastoty-1]*(1+PodstrLevel*Podstroyka+PodstrLevel*PodstrLevel*PodPodstroyka);

  delay(delayValue); LCDprint(ch);
  #if defined (timeMLS)
   timerUnSet(); timerSet(Period(ch)); 
  #else
   timerSet(Period(ch));
  #endif
}

//функция определяет задержку
unsigned long Period(double freq)
{
  #if defined(timeMSR)
    double p=1000. / 2. / freq;
  #else
    double p=1000000. /2. / freq;
  #endif
  return p;
}

unsigned long getEncoderTime(){
  //dT[0]=dT[1]; 
  dT[1]=dT[2];
  dT[2] = millis(); //micros();;
  #if defined(DEBUG)
    Serial.print("dT "); Serial.println(dT[2]-dT[1]);//(1./2.*(dT[0]-2*dT[1]+dT[2])));
  #endif
  if ((dT[2]-dT[1]) > DisplayResetPeriod) LCDprintStart();
  return (dT[2]-dT[1]);//1./2.*(dT[0]-2*dT[1]+dT[2]));

}

#define dKr 10. //Диаметр крутилки

unsigned long dTperiod[]={ 700.*dKr/10.,  500.*dKr/10.,  330.*dKr/10.,     150.*dKr/10.,      70.*dKr/10.,     32.*dKr/10.,      15.*dKr/10.,       14.*dKr/10.};
float dTshagi[] =        { 0.01,          0.05,          0.07,              0.1,                 0.5,              5,            10,               0, 0};
//unsigned long dTperiod[]={                 700.*dKr/10.,   330.*dKr/10.,     250.*dKr/10.,      170.*dKr/10.,     132.*dKr/10.,      100.*dKr/10.,  50.*dKr/10.,  30.*dKr/10.,};
//float dTshagi[] =        { 0.01,           0.1,              1,                 10,              100,            0,               1000,              0,             0};
const int dTsize = 8;
const int MAXFREQ=10000;
const int MINFREQ=0;


bool EncoderPochasovoi(){
     unsigned long t=getEncoderTime();
    if (regim==regZAPOL) {
      #if defined(DEBUG)
            Serial.print("t "); Serial.print(t);
      #endif
      for (int ii=0; ii<dTsize; ii++){
        if (t>=dTperiod[ii]){
          ch=ch-dTshagi[ii];
          #if defined(DEBUG)
            Serial.print("ii "); Serial.println(ii);
          #endif
          goto metka10;
        }
      }
metka10: ;
   }

   if (ch<MINFREQ) ch=MINFREQ;
   
   return true;
}
bool EncoderProtivchasovoi() {
   if (regim==regZAPOL){
      unsigned long t=getEncoderTime();
      //unsigned long ii=dTsize;
      #if defined(DEBUG)
            Serial.print("t "); Serial.print(t);
            //Serial.print(", ii "); Serial.println(ii);
      #endif
     for (int ii=0; ii<dTsize; ii++){
        if (t>=dTperiod[ii]){
          ch=ch+dTshagi[ii];
          goto metka20;
          #if defined(DEBUG)
            Serial.print("ii "); Serial.println(ii);
          #endif
        };
      }
metka20:  ;
   }
   if (ch>MAXFREQ) ch=MAXFREQ;
   return true;
}


void loop() {
  #if defined(useCyberLib) 
	label:
  #endif

  //считываем состояние кнопок
  //int sensorValue = analogRead(A0);
  //Serial.println(sensorValue);
  //delay(100);

  //for (int i = 0; i < 1; i++) {
  #if defined(useCyberLib) 
	ButsState[ButMainFreq]=A0_Read;
  #else
	ButsState[ButMainFreq] = analogRead(ButPins[ButMainFreq]);
  #endif
  #if defined(DEBUG)
    //Serial.println(ButsState[ButMainFreq]);
  #endif


  //Кнопка переключения режима
  if (ButsState[ButMainFreq] >= isPUSHED[ButMainFreq]) {
        #if defined(DEBUG)
         Serial.print("Ch = "); Serial.print(ch); Serial.print(" ==? "); Serial.println(nomerChastoty-1-isNomerOneMore);
        #endif

    if(regim==regZAPOL){ 
      if (ch==Chastoty[nomerChastoty-1-isNomerOneMore]) {
         regim=regPEREB;

         #if defined(DEBUG)
          Serial.println("Regim == regPEREB");
         #endif
      }
      else {
        isNomerOneMore=1;
        Chastoty[nomerChastoty-1]=ch;
        nomerChastoty++; OsnChastot++;
        if (nomerChastoty<MaxTableChastoty) {
          #if defined(DEBUG)
           Serial.print("Regim == regZAPOLN");
          #endif 
          }
        else {
          regim=regPEREB;
          nomerChastoty--; OsnChastot--;
          #if defined(DEBUG)
           Serial.println("Regim == regPEREB");
          #endif
        }
        #if defined(DEBUG)
         Serial.print(" Nomer v tabl ="); Serial.print(" OsnChastot"); Serial.println(OsnChastot);
        #endif
      }
   }
   else
     {nomerChastoty++;
      if (nomerChastoty>=OsnChastot) nomerChastoty=1;
      ch=Chastoty[nomerChastoty-1];
      isChanged=true;}

   delay(delayValue); 
  }

  //Энкодер - перебор основной частоты или делителя основной частоты
  #if defined(useCyberLib) 
	encoder_A=D11_Read;//D9_Read;
	encoder_B=D12_Read;//D10_Read;
  #else
	encoder_A = digitalRead(EncCLK);//Считываем значения выходов энкодера
	encoder_B = digitalRead(EncDT);//
  #endif
  if((!encoder_A && encoder_A_prev) && (regim==regZAPOL))  //Если уровень сигнала А низкий, и в предыдущем цикле он был высокий 
  {
    timerUnSet();
    if(encoder_B)    //Если уровень сигнала В высокий
    {//Значит вращение происходит по часовой стрелке
     isChanged=EncoderPochasovoi();
    }
    else     //Если уровень сигнала В низкий
    {        //Значит вращение происходит против часовой стрелки
     isChanged=EncoderProtivchasovoi();
    }
  }
  encoder_A_prev = encoder_A;

 //Кнопка перезагрузки
 /* if ((ButsState[ButMainFreq] <= (isPUSHED[ButChangeReg]+20)) && (ButsState[ButMainFreq] >= (isPUSHED[ButChangeReg]-20))){
    timerUnSet();
    //reset();
    resetFunc(); 
  }
 */
  if (isChanged==true) {
    isChanged=false;
    //ch=Chastoty[nomerChastoty-1]*(1+PodstrLevel*Podstroyka+PodstrLevel*PodstrLevel*PodPodstroyka);

    #if defined(DEBUG)
     Serial.print("Ch "); Serial.print(ch); Serial.print(" ");
     Serial.print("Osn.Chastota "); Serial.println(Chastoty[nomerChastoty-1]);
     Serial.print("Period "); Serial.println(Period(ch));
     Serial.print("Regim ");    Serial.println(regim);
    #endif

    LCDprint(ch);
    timerUnSet(); timerSet(Period(ch));
  }
  

 #if defined(useCyberLib)
	goto label;
 #endif
}


